import asyncio
import functools
from contextlib import contextmanager


class AsyncStateMachine:
    def __init__(self, states, init_state, loop: asyncio.AbstractEventLoop = None):

        self._loop = loop or asyncio.get_event_loop()
        self._next_transition_event = asyncio.Event(loop=self._loop)
        self._transition_in_progress = False

        if isinstance(states, (list, set)):
            state_set = set(states)
            if len(states) > len(state_set):
                raise AttributeError('No duplicate states are allowed.')
            self.states = {name: _State(name, self._loop) for name in set(states)}
            for state in self.states.values():
                state.next_states = set(self.states.values())

        elif isinstance(states, dict):
            self.states = {name: _State(name, self._loop) for name in states.keys()}
            for state in self.states.values():
                state.next_states = {self.states[name] for name in states[state.name]}

        else:
            raise TypeError('states must by of type list, set or dict. Got {}'.format(type(states)))

        try:
            self._current_state = self.states[init_state]  # type: _State
        except KeyError:
            raise KeyError('init state {} not in states'.format(init_state))

    @property
    def current_state(self):
        return self._current_state

    def _ensure_state_obj(self, arg):
        return arg if isinstance(arg, _State) else self.states[arg]

    def _ensure_state_list(self, arg):
        ret = arg if isinstance(arg, list) else [arg]
        return list(map(self._ensure_state_obj, ret))

    def _assert_legal_transition(self, from_states, to_states):
        if isinstance(from_states, list):
            [self._assert_legal_transition(s, to_states) for s in from_states]
        elif isinstance(to_states, list):
            [self._assert_legal_transition(from_states, s) for s in to_states]
        elif to_states not in from_states.next_states:
            raise IllegalTransitionError('Illegal transition from {} to {}'.format(from_states, to_states))

    def _assert_reachability(self, from_states, to_states):
        if isinstance(from_states, list):
            [self._assert_reachability(s, to_states) for s in from_states]
        elif isinstance(to_states, list):
            [self._assert_reachability(from_states, s) for s in to_states]
        elif not self._is_reachable_one_to_one(from_states, to_states):
            raise StateUnreachableError('State {} cannot reach state {}.'.format(from_states, to_states))

    @functools.lru_cache(maxsize=32)
    def _is_reachable_one_to_one(self, from_state, to_state):
        if isinstance(to_state, str):
            to_state = self.states[to_state]
        if isinstance(from_state, str):
            from_state = self.states[from_state]

        states_reached = set()
        states_todo = {from_state}
        while len(states_todo) > 0:
            working_state = states_todo.pop()
            for next_state in working_state.next_states:
                if next_state not in states_reached | states_todo | {working_state}:
                    if next_state is to_state:
                        return True
                    states_todo.add(next_state)
            states_reached.add(working_state)
        return False

    def is_reachable(self, from_state, to_state) -> bool:
        if isinstance(from_state, list):
            return all([self.is_reachable(s, to_state) for s in from_state])
        if isinstance(to_state, list):
            return all([self.is_reachable(from_state, s) for s in to_state])

        return self._is_reachable_one_to_one(from_state, to_state)

    def transition(self, to_state, from_state=None):
        if self._transition_in_progress:
            raise TransitionInProgressError()

        to_state = self._ensure_state_obj(to_state)
        from_state = self._ensure_state_list(from_state or self.current_state)

        self._assert_legal_transition(from_state, to_state)

        self._current_state = to_state
        self._next_transition_event.set()
        self._next_transition_event = asyncio.Event(loop=self._loop)

    @contextmanager
    def transition_context(self, source_state, target_state, error_state=None):
        if self._transition_in_progress:
            raise TransitionInProgressError()

        source_state = self._ensure_state_list(source_state)
        target_state = self._ensure_state_obj(target_state)
        if error_state:
            error_state = self._ensure_state_obj(error_state)

        if self.current_state not in source_state:
            raise IllegalTransitionError('Current state {} is not in {}'.format(self.current_state, source_state))
        self._assert_reachability(source_state, [target_state] + ([error_state] if error_state else []))

        self._transition_in_progress = True
        try:
            yield
            raised_exc = None
        except Exception as e:
            raised_exc = e
        self._transition_in_progress = False

        if raised_exc:
            if error_state:
                self.transition(error_state)
            raise raised_exc
        else:
            self.transition(target_state)

    def waiting_transition_context(self, source_state, target_state, error_state=None):
        if self._transition_in_progress:
            raise TransitionInProgressError()

        source_state = self._ensure_state_list(source_state)
        target_state = self._ensure_state_obj(target_state)
        if error_state:
            error_state = self._ensure_state_obj(error_state)

        self._assert_reachability(source_state, [target_state] + ([error_state] if error_state else []))

        class _Helper:
            def __init__(self, state_machine: AsyncStateMachine):
                self.state_machine = state_machine

            async def __aenter__(self):
                await self.state_machine.wait_for_state(source_state)
                self.state_machine._transition_in_progress = True

            async def __aexit__(self, exc_type, exc_val, exc_tb):
                self.state_machine._transition_in_progress = False
                if exc_type:
                    raised_exc = exc_val
                else:
                    raised_exc = None

                if raised_exc:
                    if error_state:
                        self.state_machine.transition(error_state)
                    raise raised_exc
                else:
                    self.state_machine.transition(target_state)

        return _Helper(self)

    async def wait_for_state(self, state):
        state = self._ensure_state_list(state)

        while True:
            if self._current_state in state:
                return self._current_state
            self._assert_reachability(self.current_state, state)
            await self._next_transition_event.wait()


class _State:
    def __init__(self, name: str, loop: asyncio.AbstractEventLoop):
        self.name = name
        self.next_states = set()

    def __eq__(self, other):
        return self is other or self.name == other

    def __hash__(self):
        return self.name.__hash__()

    def __str__(self):
        return self.name


class IllegalTransitionError(Exception):
    pass


class StateUnreachableError(Exception):
    pass


class TransitionInProgressError(Exception):
    pass
