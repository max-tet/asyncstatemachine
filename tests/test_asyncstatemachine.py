import asyncio

import pytest

from asyncstatemachine import AsyncStateMachine, IllegalTransitionError, StateUnreachableError, \
    TransitionInProgressError


def test_list_construction():
    statelist = ['a', 'b', 'c']
    sm = AsyncStateMachine(statelist, statelist[0])

    assert sm.current_state == statelist[0]
    for state in statelist:
        assert state in sm.states
        for nstate in statelist:
            assert nstate in map(lambda s: s.name, sm.states[state].next_states)


def test_set_construction():
    stateset = {'a', 'b', 'c'}
    sm = AsyncStateMachine(stateset, 'a')

    assert sm.current_state == 'a'
    for state in stateset:
        assert state in sm.states
        for nstate in stateset:
            assert nstate in map(lambda s: s.name, sm.states[state].next_states)


def test_dict_construction():
    statedict = {
        'a': ['a', 'b'],
        'b': ['a', 'b'],
        'c': ['c']
    }
    sm = AsyncStateMachine(statedict, 'a')

    assert sm.current_state == 'a'
    for state, next_states in statedict.items():
        assert state in sm.states
        for nstate in next_states:
            assert nstate in map(lambda s: s.name, sm.states[state].next_states)


def test_list_construction_with_duplicates():
    with pytest.raises(AttributeError):
        AsyncStateMachine(['a', 'a'], 'a')


def test_transition_by_string():
    sm = AsyncStateMachine(['a', 'b'], 'a')
    sm.transition('b')
    assert sm.current_state == 'b'


def test_transition_by_obj():
    sm = AsyncStateMachine(['a', 'b'], 'a')
    sm.transition(sm.states['b'])
    assert sm.current_state == 'b'


def test_self_transition():
    sm = AsyncStateMachine(['a', 'b'], 'a')
    sm.transition('a')
    assert sm.current_state == 'a'


def test_illegal_transition():
    sm = AsyncStateMachine({'a': [], 'b': []}, 'a')
    with pytest.raises(IllegalTransitionError):
        sm.transition('b')


def test_reachability():
    sm = AsyncStateMachine({'a': [], 'b': ['a']}, 'a')
    assert sm.is_reachable('b', 'a')


def test_non_reachability():
    sm = AsyncStateMachine({'a': [], 'b': ['a']}, 'a')
    assert not sm.is_reachable('a', 'b')


def test_wait():
    loop = asyncio.new_event_loop()
    sm = AsyncStateMachine(['a', 'b'], 'a', loop=loop)
    exec_indicator = {'a': False, 'b': False}

    async def wait_for_a():
        await sm.wait_for_state('a')
        exec_indicator['a'] = True
        loop.stop()

    async def wait_for_b():
        await sm.wait_for_state('b')
        exec_indicator['b'] = True
        loop.stop()

    async def transition():
        sm.transition('b')

    asyncio.ensure_future(wait_for_b(), loop=loop)
    asyncio.ensure_future(wait_for_a(), loop=loop)
    loop.run_forever()
    assert exec_indicator['a']
    assert not exec_indicator['b']
    assert sm.current_state == 'a'

    asyncio.ensure_future(transition(), loop=loop)
    loop.run_forever()
    assert exec_indicator['b']
    assert sm.current_state == 'b'

    loop.close()


def test_wait_unreachable():
    loop = asyncio.new_event_loop()
    sm = AsyncStateMachine({'a': [], 'b': []}, 'a', loop=loop)

    async def wait_for_b():
        await sm.wait_for_state('b')

    with pytest.raises(StateUnreachableError):
        loop.run_until_complete(wait_for_b())

    loop.close()


def test_wait_multi():
    loop = asyncio.new_event_loop()
    sm = AsyncStateMachine(['a', 'b', 'c'], 'a', loop=loop)
    exec_indicator = {'bc': False}

    async def wait_for_bc():
        assert await sm.wait_for_state(['b', 'c']) == 'c'
        exec_indicator['bc'] = True

    async def transition():
        sm.transition('c')

    asyncio.ensure_future(transition(), loop=loop)
    loop.run_until_complete(wait_for_bc())
    assert exec_indicator['bc']
    assert sm.current_state == 'c'

    loop.close()


def test_wait_then_unreachable():
    loop = asyncio.new_event_loop()
    sm = AsyncStateMachine({'a': ['b', 'c'], 'b': [], 'c': []}, 'a', loop=loop)

    async def wait_for_c():
        assert await sm.wait_for_state('c') == 'c'

    async def transition():
        sm.transition('b')

    asyncio.ensure_future(transition(), loop=loop)
    with pytest.raises(StateUnreachableError):
        loop.run_until_complete(wait_for_c())

    loop.close()


def test_contextmanager_transition():
    sm = AsyncStateMachine(['a', 'b'], 'a')
    with sm.transition_context('a', 'b'):
        assert sm.current_state == 'a'
    assert sm.current_state == 'b'


def test_contextmanager_transition_wrong_source():
    sm = AsyncStateMachine(['a', 'b'], 'a')
    with pytest.raises(IllegalTransitionError):
        with sm.transition_context('b', 'a'):
            pass


def test_contextmanager_transition_unreachable():
    sm = AsyncStateMachine({'a': [], 'b': []}, 'a')
    with pytest.raises(StateUnreachableError):
        with sm.transition_context('a', 'b'):
            pass


def test_contextmanager_transition_unreachable_multi():
    sm = AsyncStateMachine({'a': ['c'], 'b': [], 'c': []}, 'a')
    with pytest.raises(StateUnreachableError):
        with sm.transition_context(['a', 'b'], 'c'):
            pass


def test_contextmanager_transition_in_progress():
    sm = AsyncStateMachine(['a', 'b', 'c'], 'a')
    with pytest.raises(TransitionInProgressError):
        with sm.transition_context(['a', 'b'], 'c'):
            sm.transition('b')


def test_contextmanager_transition_error_state():
    sm = AsyncStateMachine(['a', 'b', 'e'], 'a')
    with pytest.raises(Exception):
        with sm.transition_context('a', 'b', error_state='e'):
            raise Exception()
    assert sm.current_state == 'e'


def test_acontextmanager_transition():
    loop = asyncio.new_event_loop()
    sm = AsyncStateMachine(['a', 'b'], 'a', loop=loop)

    async def transition():
        async with sm.waiting_transition_context('a', 'b'):
            assert sm.current_state == 'a'
        assert sm.current_state == 'b'

    loop.run_until_complete(transition())
    loop.close()


def test_acontextmanager_transition_wait():
    loop = asyncio.new_event_loop()
    sm = AsyncStateMachine(['a', 'b', 'c'], 'a', loop=loop)
    exec_indicator = {'b': False, 'c': False}

    async def wait_for_a():
        await sm.wait_for_state('a')
        exec_indicator['a'] = True

    async def transition_ab():
        sm.transition('b')
        await sm.wait_for_state('c')

    async def transition_bc():
        async with sm.waiting_transition_context('b', 'c'):
            exec_indicator['b'] = True
        exec_indicator['c'] = True

    asyncio.ensure_future(transition_bc(), loop=loop)
    loop.run_until_complete(wait_for_a())
    assert not exec_indicator['b'] and not exec_indicator['c']
    assert sm.current_state == 'a'
    loop.run_until_complete(transition_ab())
    assert exec_indicator['b'] and exec_indicator['c']
    assert sm.current_state == 'c'
    loop.close()


def test_acontextmanager_transition_wait_multi():
    loop = asyncio.new_event_loop()
    sm = AsyncStateMachine(['a', 'b', 'c'], 'a', loop=loop)

    async def wait_for_bc():
        await sm.wait_for_state(['b', 'c'])

    async def transition_ac():
        sm.transition('c')

    asyncio.ensure_future(transition_ac(), loop=loop)
    loop.run_until_complete(wait_for_bc())
    loop.close()


def test_acontextmanager_transition_wait_then_unreachable():
    loop = asyncio.new_event_loop()
    sm = AsyncStateMachine({'a': ['b', 'c'], 'b': [], 'c': []}, 'a', loop=loop)

    async def wait_for_c():
        await sm.wait_for_state('c')

    async def transition():
        sm.transition('b')

    asyncio.ensure_future(transition(), loop=loop)
    with pytest.raises(StateUnreachableError):
        loop.run_until_complete(wait_for_c())

    loop.close()


def test_acontextmanager_transition_unreachable():
    loop = asyncio.new_event_loop()
    sm = AsyncStateMachine({'a': [], 'b': []}, 'a', loop=loop)

    async def transition():
        with pytest.raises(StateUnreachableError):
            async with sm.waiting_transition_context('a', 'b'):
                pass

    loop.run_until_complete(transition())
    loop.close()


def test_acontextmanager_transition_unreachable_multi():
    loop = asyncio.new_event_loop()
    sm = AsyncStateMachine({'a': ['c'], 'b': [], 'c': []}, 'a', loop=loop)

    async def transition():
        with pytest.raises(StateUnreachableError):
            async with sm.waiting_transition_context(['a', 'b'], 'c'):
                pass

    loop.run_until_complete(transition())
    loop.close()


def test_acontextmanager_transition_external_state_change():
    loop = asyncio.new_event_loop()
    sm = AsyncStateMachine(['a', 'b', 'c'], 'a', loop=loop)

    async def transition():
        with pytest.raises(TransitionInProgressError):
            async with sm.waiting_transition_context(['a', 'b'], 'c'):
                sm.transition('b')

    loop.run_until_complete(transition())
    loop.close()


def test_acontextmanager_transition_error_state():
    loop = asyncio.new_event_loop()
    sm = AsyncStateMachine(['a', 'b', 'e'], 'a', loop=loop)

    async def transition():
        with pytest.raises(Exception):
            async with sm.waiting_transition_context('a', 'b', error_state='e'):
                raise Exception()
        assert sm.current_state == 'e'

    loop.run_until_complete(transition())
    loop.close()
