from distutils.core import setup

from setuptools import find_packages

setup(
    name='asyncstatemachine',
    version='1.0.0',
    packages=find_packages(exclude=["tests"]),
    url='',
    license='',
    author='Max von Tettenborn',
    author_email='max@vtettenborn.net',
    description='A state machine integrated with asyncio',
    extras_require={
        'dev': [
            'pytest'
        ]
    }
)
